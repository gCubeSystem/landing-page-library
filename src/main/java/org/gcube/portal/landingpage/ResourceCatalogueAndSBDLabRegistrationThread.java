package org.gcube.portal.landingpage;

import java.util.Date;
import java.util.List;

import org.gcube.portal.notifications.thread.NewUserSiteRegistrationNotificationThread;
import org.gcube.vomanagement.usermanagement.UserManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayRoleManager;
import org.gcube.vomanagement.usermanagement.impl.LiferayUserManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.liferay.portal.kernel.exception.SystemException;
import com.liferay.portal.model.Group;
import com.liferay.portal.model.User;
import com.liferay.portal.service.GroupLocalServiceUtil;
/**
 * 
 * @author massi
 * register the user to the group having the given group id
 */
public class ResourceCatalogueAndSBDLabRegistrationThread implements Runnable {
	private static Logger _log = LoggerFactory.getLogger(ResourceCatalogueAndSBDLabRegistrationThread.class);
	private static long RESOURCE_CATALOGUE_GROUPID = 459909;

	private User user;
	private Group resourceCatalogueGroup;
	private String siteURL;
	List<Group> userSites;
	UserManager um;
	
	public ResourceCatalogueAndSBDLabRegistrationThread(List<Group> userSites, User user, String siteURL) {
		super();
		this.user = user;
		this.userSites = userSites;
		this.siteURL = siteURL;
		um = new LiferayUserManager();
		try {
			resourceCatalogueGroup = GroupLocalServiceUtil.getGroup(RESOURCE_CATALOGUE_GROUPID);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		boolean isRegisteredRC = false;

		for (Group group : userSites) {
			if (group.getGroupId() == resourceCatalogueGroup.getGroupId()) {
				isRegisteredRC = true;
				_log.debug("user " +  user.getFullName() + " is already registered to the VRE " + resourceCatalogueGroup.getName());
				break;
			}
		}
		if (!isRegisteredRC) {
			try {
				_log.info("SBD Hook " +  user.getFullName() + " going to be registered to the VRE " + resourceCatalogueGroup.getName());
				registerUserToVRE(user, resourceCatalogueGroup, siteURL);
			} catch (SystemException e) {
				_log.error("Could not register to "+ resourceCatalogueGroup.getName());
				e.printStackTrace();
			}
		}		
	}
	
	/**
	 * the user to the VRE, plus send notifications to the vre manages of the vre
	 * in order to register a user i had to create a fake membership request because assigning a user to a group would have required
	 * the user to logout and login otherwise
	 */
	protected void registerUserToVRE(User user, Group vre, String siteURL) throws SystemException {
		try {			
			um.requestMembership(user.getUserId(), vre.getGroupId(), "Automatic Request at " + new Date());
			_log.info("fakeRequest sent");
			String replierUsername = LiferayUserManager.getAdmin().getScreenName();
			_log.trace("Sleep 1 second ...");
			Thread.sleep(2000);
			um.acceptMembershipRequest(user.getUserId(), vre.getGroupId(), true, replierUsername, "Automatic acceptance request at " + new Date());
			_log.info("fakeRequest accepted");
			_log.info("User " +  user.getScreenName() +" automatically registered to " + vre.getName());
			Thread emailSiteManagersThread = new Thread(new NewUserSiteRegistrationNotificationThread(new LiferayUserManager(), new LiferayRoleManager() ,user, vre, siteURL));
			emailSiteManagersThread.start();
		}catch (Exception e) {
			_log.error("registerUserToVRE FAILED for: "+ vre.getName());
			e.printStackTrace();
		}

	}

}

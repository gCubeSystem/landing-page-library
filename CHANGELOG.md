
# Changelog for landing-page-library

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [v1.4.0] - 2021-01-07

Feature #20395 - Remove user automatic registration to SoBigDataLab VRE from landing-page hook

## [v1.3.1] - 2020-04-21

Fix for Bug #19138, landing page hook register VRE users automatically if a vhost is associated to the VRE

Ported to git

## [v1.0.0] - 2016-06-22

First release 
